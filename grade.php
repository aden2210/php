<?php
    if (isset($_POST['submit'])){
    $komposisiuts =$_POST['uts']*0.35;
    $komposisiuas =$_POST['uas']*0.50;
    $komposisitugas =$_POST['tugas']*0.15;
    $totalnilai =$komposisiuts +$komposisitugas +$komposisiuas; 
    if ($totalnilai>=90 && $totalnilai<=100) {
        $grade = "A";
    }
    elseif ($totalnilai>70 && $totalnilai<90) {
        $grade = "B";
    }
    elseif ($totalnilai>50 && $totalnilai<=70) {
        $grade = "C";
    }
    elseif ($totalnilai<=50) {
        $grade = "D";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous" />

    <title>Grade Generator</title>
  </head>
  <body class="bg-light text-dark">
    <div class="container border rounded border-info border-3 shadow-sm mt-5 mb-5 p-5">
        <h1 class="text-center">GRADE GENERATOR</h1>
        <div class="row justify-content-center ">
            <div class="col-8 p-5">
                <form action="" method="POST">
                    <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" >
                    </div>
                    <div class="mb-3">
                    <label for="mapel" class="form-label">Mata Pelajaran</label>
                    <input type="text" name="mapel" class="form-control" id="mapel" >
                    </div>
                    <div class="mb-3">
                    <label for="uts" class="form-label">Nilai UTS</label>
                    <input type="number" name="uts" class="form-control" id="uts" />
                    </div>
                    <div class="mb-3">
                    <label for="uas" class="form-label">Nilai UAS</label>
                    <input type="number" name="uas" class="form-control" id="uas" />
                    </div>
                    <div class="mb-3">
                    <label for="tugas" class="form-label">Nilai Tugas</label>
                    <input type="number" name="tugas" class="form-control" id="tugas" />
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <?php if(isset($_POST['submit'])): ?>
        <section class="border rounded border-info border-3 shadow-sm">
        <h1 class="text-center">RESULT</h1>
            <div class="container p-5">
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Nama</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['nama']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Mata Pelajaran</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['mapel']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Nilai UTS</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['uts']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Nilai UAS</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $_POST['uas']?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Total Nilai</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $totalnilai?></p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-4">
                    <p>Grade Nilai</p>
                    </div>
                    <div class="col-4">
                    <p>: <?php echo $grade?></p>
                    </div>
                </div>
            </div>
        <section>
        <?php endif; ?>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>
